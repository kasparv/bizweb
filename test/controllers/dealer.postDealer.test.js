const assert = require('chai').assert;
const Promise = require('bluebird');
const simple = require('simple-mock');

const dealer = require('../../controllers/dealer');
const Dealer = require('../../models/dealer');

describe('Controller dealer.postDealer', function () {

  afterEach(function () {
    simple.restore();
  });

  it('should return success true if creating new user is successful', done => {
    let mDealerSave = simple.mock(Dealer, 'newDealer', () => {
      return {
        save() {
          return Promise.resolve();
        }
      };
    });
    let req = {body: {name: 'foo'}, user: {_id: 'baz'}};
    let res = {
      json(inObj) {
        assert.equal(inObj.success, true);
        assert.equal(mDealerSave.called, 1);
        done();
      }
    };
    dealer.postDealer(req, res);
  });

  it('should return 400 if invalid request payload', done => {
    let req = {body: {}};
    let res = {
      status(httpCode) {
        assert.equal(httpCode, 400);
        return {
          json(inObj) {
            assert.equal(inObj.message, 'Invalid');
            done();
          }
        };
      }
    };
    dealer.postDealer(req, res);
  });

  it('should return success false if creating new user fails internally', done => {
    let mDealerSave = simple.mock(Dealer, 'newDealer', () => {
      return {
        save() {
          return Promise.reject(new Error('log error'));
        }
      };
    });
    let req = {body: {name: 'foo'}, user: {_id: 'baz'}};
    let res = {
      json(inObj) {
        assert.equal(inObj.success, false);
        assert.equal(mDealerSave.called, 1);
        done();
      }
    };
    dealer.postDealer(req, res);
  });

});


