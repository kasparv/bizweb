import { Injectable } from '@angular/core';
import { Http, Response } from '@angular/http';

import { BaseDao } from './base-dao';
import { Dealer } from './entity/dealer';

@Injectable()
export class DealerService extends BaseDao {

  constructor(private http: Http) {
    super();
  }

  getList() {
    return this.http
      .get('http://localhost:3333/api/dealer', this.jwt())
      .map((response: Response) => {
        return response.json();
      });
  }

  create(dealer: Dealer) {
    return this.http
      .post('http://localhost:3333/api/dealer', dealer, this.jwt())
      .map((response: Response) => response.json());
  }

  update(dealer: Dealer) {
    return this.http
      .put('http://localhost:3333/api/dealer/' + dealer._id, dealer, this.jwt())
      .map((response: Response) => response.json());
  }

  delete(_id: string) {
    return this.http
      .delete('http://localhost:3333/api/dealer/' + _id, this.jwt())
      .map((response: Response) => response.json());
  }

  getView() {
    return this.http
      .get('http://localhost:3333/api/dealer-view', this.jwt())
      .map((response: Response) => {
        return response.json();
      });
  }
}
