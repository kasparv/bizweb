import { Department, Biz } from './index';

export class Depbiz {
    _id: string;
    department: Department;
    biz: Biz;
}
