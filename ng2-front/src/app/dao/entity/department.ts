import { Dealer } from './index';

export class Department {
    _id: string;
    name: string;
    dealer: Dealer;
}
