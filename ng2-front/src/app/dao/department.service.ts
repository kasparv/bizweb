import { Injectable } from '@angular/core';
import { Http, Response } from '@angular/http';

import { BaseDao } from './base-dao';
import { Department } from './entity/department';

@Injectable()
export class DepartmentService extends BaseDao {

  constructor(private http: Http) {
    super();
  }

  getList() {
    return this.http
      .get('http://localhost:3333/api/department', this.jwt())
      .map((response: Response) => {
        return response.json();
      });
  }

  create(department: Department) {
    return this.http
      .post('http://localhost:3333/api/department', department, this.jwt())
      .map((response: Response) => response.json());
  }

  update(department: Department) {
    return this.http
      .put('http://localhost:3333/api/department/' + department._id, department, this.jwt())
      .map((response: Response) => response.json());
  }

  delete(_id: string) {
    return this.http
      .delete('http://localhost:3333/api/department/' + _id, this.jwt())
      .map((response: Response) => response.json());
  }

}
