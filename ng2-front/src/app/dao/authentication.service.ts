import { Injectable } from '@angular/core';
import { Http, Headers, Response } from '@angular/http';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/map';

@Injectable()
export class AuthenticationService {

  constructor(private http: Http) { }

  login(username: string, password: string) {

    let url: string = 'http://localhost:3333/api/login',
      headers: Headers = new Headers();

    headers.append("Authorization", "Basic " + btoa(username + ":" + password));
    headers.append("Content-Type", "application/json");

    return this.http
      .post(url, {}, { headers: headers })
      .map((response: Response) => {
        let result = response.json();
        if (result && result.success && result.token) {
          localStorage.setItem('currentUser', JSON.stringify(result));
        } else {
          console.log('no success');
        }
      });
  }

  logout() {
    localStorage.clear();
  }

}
