import { Injectable } from '@angular/core';
import { Http, Response } from '@angular/http';

import { BaseDao } from './base-dao';
import { Biz } from './entity/biz';

@Injectable()
export class BizService extends BaseDao {

  constructor(private http: Http) {
    super();
  }

  create(name: string, file: File) {
    let input = new FormData();
    input.append("file", file);
    input.append("name", name);
    return this.http
      .post('http://localhost:3333/api/biz', input, this.jwt())
      .map((response: Response) => response.json());
  }

  getList() {
    return this.http
      .get('http://localhost:3333/api/biz', this.jwt())
      .map((response: Response) => {
        return response.json();
      });
  }

  delete(_id: string) {
    return this.http
      .delete('http://localhost:3333/api/biz/' + _id, this.jwt())
      .map((response: Response) => response.json());
  }

}
