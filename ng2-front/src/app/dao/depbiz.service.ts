import { Injectable } from '@angular/core';
import { Http, Response } from '@angular/http';

import { BaseDao } from './base-dao';
import { Depbiz } from './entity/depbiz';

@Injectable()
export class DepbizService extends BaseDao {

  constructor(private http: Http) {
    super();
  }

  getList() {
    return this.http
      .get('http://localhost:3333/api/depbiz', this.jwt())
      .map((response: Response) => {
        return response.json();
      });
  }

  create(depbiz: Depbiz) {
    return this.http
      .post('http://localhost:3333/api/depbiz', depbiz, this.jwt())
      .map((response: Response) => response.json());
  }

  delete(_id: string) {
    return this.http
      .delete('http://localhost:3333/api/depbiz/' + _id, this.jwt())
      .map((response: Response) => response.json());
  }

}
