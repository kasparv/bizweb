import { Component, OnInit } from '@angular/core';
import { HeaderMenuComponent } from '../../shared/header-menu/header-menu.component';

import { Biz } from '../../dao/entity/biz';
import { BizService } from '../../dao/biz.service';

@Component({
  selector: 'app-biz',
  templateUrl: './biz.component.html',
  styleUrls: ['./biz.component.scss']
})
export class BizComponent implements OnInit {

  model: Biz = new Biz();
  models: Biz[] = [];
  file: File;

  private getModels() {
    this.modelService
      .getList()
      .subscribe(result => {
        this.models = result.data;
      });
  }

  private insertModel() {
    if (this.model.name && this.file && this.file.type === 'application/zip') {
      this.modelService
        .create(this.model.name, this.file)
        .subscribe(result => {
          this.resetModel();
          this.getModels();
        });
    }
  }

  private deleteModel(_id: string) {
    this.modelService
      .delete(_id)
      .subscribe(result => {
        if (result.success) {
          this.getModels();
        }
      });
  }

  constructor(private modelService: BizService) { }

  ngOnInit() {
    this.getModels();
  }

  resetModel() {
    this.model = new Biz();
    this.file = null;
  }

  addModel(fileupload: any) {
    if (fileupload.files.length) {
      this.file = fileupload.files[0];
      this.insertModel();
    }
  }

  removeModel(_id: string) {
    this.deleteModel(_id);
  }

}