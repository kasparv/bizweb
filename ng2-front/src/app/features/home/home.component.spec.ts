/* tslint:disable:no-unused-variable */
import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { By } from '@angular/platform-browser';
import { DebugElement } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { APP_BASE_HREF } from '@angular/common';

import { routing } from '../../app.routing';

import { HeaderMenuComponent } from '../../shared/header-menu/header-menu.component';
import { BizComponent } from '../biz/biz.component';
import { DepartmentComponent } from '../department/department.component';
import { DepbizComponent } from '../depbiz/depbiz.component';
import { LoginComponent } from '../login/login.component';
import { DealerComponent } from '../dealer/dealer.component';

import { Dealer } from '../../dao/entity/dealer';
import { DealerService } from '../../dao/dealer.service';

import { HomeComponent } from './home.component';

describe('HomeComponent', () => {
  let component: HomeComponent;
  let fixture: ComponentFixture<HomeComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [
        HomeComponent,
        BizComponent,
        DealerComponent,
        DepartmentComponent,
        DepbizComponent,
        LoginComponent,
        HeaderMenuComponent
      ],
      imports: [
        routing,
        FormsModule
      ],
      providers: [
        { provide: APP_BASE_HREF, useValue: '/' },
        Dealer,
        DealerService
      ]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(HomeComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
