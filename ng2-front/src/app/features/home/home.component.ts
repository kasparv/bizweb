import { Component, OnInit } from '@angular/core';

import { Dealer } from '../../dao/entity/dealer';
import { DealerService } from '../../dao/dealer.service';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss']
})
export class HomeComponent implements OnInit {

  isClassVisible: false;

  model: Dealer = new Dealer();
  models: Dealer[] = [];

  constructor(private modelService: DealerService) { }

  ngOnInit() {console.log('ngOnInit');
    this.loadModels();
  }

  loadModels() {
    this.modelService
      .getView()
      .subscribe(result => {
        this.models = result.data;
        console.log(this.models);
      });
  }

}
