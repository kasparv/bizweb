import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';

import { AuthenticationService } from '../../dao/authentication.service';
import { User } from '../../dao/entity/user';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {

  model: User = new User;
  loading = false;
  returnUrl: string;
  loginError: boolean = false;

  constructor(
    private route: ActivatedRoute,
    private router: Router,
    private authenticationService: AuthenticationService
  ) { }

  ngOnInit() {
    this.authenticationService.logout();
    this.returnUrl = this.route.snapshot.queryParams['returnUrl'] || '/';
  }

  login() {
    this.loginError = false;
    this.loading = true;
    this.authenticationService
      .login(this.model.username, this.model.password)
      .subscribe(
      data => {
        this.router.navigate([this.returnUrl] || ['/']);
      },
      error => {
        this.loading = false;
        this.loginError = true;
      });
  }

}
