/* tslint:disable:no-unused-variable */

import { TestBed, async } from '@angular/core/testing';

import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { HttpModule } from '@angular/http';

import { AppComponent } from './app.component';

import { APP_BASE_HREF } from '@angular/common';

import { routing } from './app.routing';

import { AuthGuard } from './shared/auth-guard';

import {
  BizService,
  DealerService,
  DepartmentService,
  DepbizService,
  AuthenticationService
} from './dao/index';

import { HomeComponent } from './features/home/home.component';
import { BizComponent } from './features/biz/biz.component';
import { DealerComponent } from './features/dealer/dealer.component';
import { DepartmentComponent } from './features/department/department.component';
import { DepbizComponent } from './features/depbiz/depbiz.component';
import { LoginComponent } from './features/login/login.component';

import { AlertModule } from 'ng2-bootstrap';
import { HeaderMenuComponent } from './shared/header-menu/header-menu.component';

describe('AppComponent', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      declarations: [
        AppComponent,
        HomeComponent,
        BizComponent,
        DealerComponent,
        DepartmentComponent,
        DepbizComponent,
        LoginComponent,
        HeaderMenuComponent
      ],
      imports: [
        BrowserModule,
        FormsModule,
        HttpModule,
        routing,
        AlertModule.forRoot()
      ],
      providers: [
        { provide: APP_BASE_HREF, useValue: '/' },
        AuthenticationService,
        AuthGuard,
        BizService,
        DealerService,
        DepartmentService,
        DepbizService
      ]
    });
    TestBed.compileComponents();
  });

  it('should create the app', async(() => {
    let fixture = TestBed.createComponent(AppComponent);
    let app = fixture.debugElement.componentInstance;
    expect(app).toBeTruthy();
  }));

  it(`should have as title 'BizWeb'`, async(() => {
    let fixture = TestBed.createComponent(AppComponent);
    let app = fixture.debugElement.componentInstance;
    expect(app.title).toEqual('BizWeb');
  }));

  it('should render title in a h1 tag', async(() => {
    let fixture = TestBed.createComponent(AppComponent);
    fixture.detectChanges();
    let compiled = fixture.debugElement.nativeElement;
    expect(compiled.querySelector('h1').textContent).toContain('BizWeb');
  }));

});
