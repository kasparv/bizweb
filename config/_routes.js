const THE_VERSION = '0.0.10';

const express = require('express');
const router = express.Router();
const authController = require('../controllers/auth');
const userController = require('../controllers/user');
const dealerController = require('../controllers/dealer');
const departmentController = require('../controllers/department');
const modelUploadController = require('../controllers/modelupload');

//Public
router.get('/', (req, res) => {
  res.json({message: THE_VERSION, data: {}});
});

//Basic AUTH
router.route('/login')
  .post(authController.isAuthenticated, authController.logIn);

router.route('/user')
  .post(authController.hasJWT, userController.postUser)
  .get(authController.hasJWT, userController.getUsers);

//JWT required

//Dealer
router.route('/dealer')
  .post(authController.hasJWT, dealerController.postDealer)
  .get(authController.hasJWT, dealerController.getDealers);

router.route('/dealer/:dealerId')
  .get(authController.hasJWT, dealerController.getDealer)
  .put(authController.hasJWT, dealerController.putDealer)
  .delete(authController.hasJWT, dealerController.deleteDealer);

//Department
router.route('/department')
  .post(authController.hasJWT, departmentController.postDepartment)
  .get(authController.hasJWT, departmentController.getDepartments);

router.route('/department/:departmentId')
  .put(authController.hasJWT, departmentController.putDepartment)
  .get(authController.hasJWT, departmentController.getDepartment)
  .delete(authController.hasJWT, departmentController.deleteDepartment);

//ModelUpload
router.route('/modelupload')
  .post(authController.hasJWT, modelUploadController.postModelUpload)
  .get(authController.hasJWT, modelUploadController.getModelUploads);


module.exports = router;


