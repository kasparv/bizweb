# BizWeb - work in progress

## Purpose

This app, expressjs + ng2, is used to serve html5 projects.  

User in role manager

* can upload zipped directory containing index.html and possible assets  
* can create dealers, departments and relate html5 projects to departments
  
User in role reader

* can browse presented structure 
* can view html5 projects as webpages associated to departments 

## TODO

Deadline 31st of January  

* ~~Switch to angular-cli~~
* Dockerize for staging & production
* ~~Continue/Complete logic development~~
* Implement layout/visual, work of Kaspar
* Lock packages with yarn
