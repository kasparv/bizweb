const Promise = require('bluebird');
const formidable = require('formidable');
const path = require('path');
const fs = Promise.promisifyAll(require('fs-extra'));
const extract = require('extract-zip');
const extractAsync = Promise.promisify(extract);
const slugIt = require('slug');

const BizModel = require('../models/biz');
const DepBiz = require('../models/depbiz');

const uploadDir = path.join(__dirname, '..', 'uploads');
const extractDir = path.join(__dirname, '..', 'public');

const deleteByPath = function (pathToRemove) {
  return fs.removeAsync(pathToRemove)
    .then(() => [true, null])
    .catch(err => {
      console.warn(err);
      return [false, err];
    });
};

exports.addModel = function (req, res) {

  const opts = {
    multiples: false,
    uploadDir: uploadDir
  };

  let formObj, files, fileRef, fileObject, fileNameParts, dirSlug, unzipDir;

  const parseForm = function () {

    return new Promise((resolve, reject) => {
      let form = new formidable.IncomingForm(opts);
      form.parse(req, (err, fields, files) => {
        if (err) {
          return reject(err);//return stops calling following resolve
        }
        formObj = { fields: fields, files: files };
        resolve([true]);
      });
    });

  };

  const validateUpload = function () {

    files = formObj.files;
    fileRef = Object.keys(formObj.files);

    if (!fileRef.length) {//no file
      res.json({ success: false, msg: 'FILE_MISSING', code: 400, data: {} });
      return [false];
    }

    if (!formObj.fields.name) {
      res.json({ success: false, msg: 'NAME_MISSING', code: 400, data: {} });
      deleteByPath(fileObject.path);
      return [false];
    }

    fileObject = files[fileRef[0]];
    fileNameParts = fileObject.name.split('.');

    if (fileNameParts[fileNameParts.length - 1] !== 'zip') {//incorrect extension
      res.json({ success: false, msg: 'FILE_INCORRECT_EXTENSION', code: 400, data: {} });
      deleteByPath(fileObject.path);
      return [false];
    }

    if (fileObject.type !== 'application/zip') {//incorrect mime type
      res.json({ success: false, msg: 'FILE_INCORRECT_TYPE', code: 400, data: {} });
      deleteByPath(fileObject.path);
      return [false];
    }

    return [true];
  };

  const checkIfNew = function (success) {
    if (!success) {
      return [false];
    }

    dirSlug = slugIt(formObj.fields.name).toLowerCase();
    unzipDir = path.join(extractDir, dirSlug);

    return fs
      .readdirAsync(unzipDir)
      .then(result => {
        if (result.length === 1) {//we have a dir move it up to one step
          res.json({ success: false, msg: 'CHANGE_NAME_FIELD', code: 400, data: {} });
          deleteByPath(fileObject.path);
          return [false];
        }
        return [true];//is new, slug is ok?
      })
      .catch(() => {
        return [true];//seems ok to fail here, did not exist
      });
  };

  const extractBizModel = function (success) {
    if (!success) {
      return [false];
    }

    return extractAsync
      .call(extract, fileObject.path, { dir: unzipDir })
      .then(() => {
        deleteByPath(fileObject.path);
        return fs
          .readdirAsync(unzipDir)
          .then(result => {
            if (result.length === 1) {//we have only single result it can be a file or  a directory
              let cpPath = path.join(unzipDir, result[0]);
              return fs.lstatAsync(cpPath)
                .then(stats => {
                  if (stats.isDirectory()) {
                    return fs
                      .copyAsync(cpPath, unzipDir, { clobber: true })
                      .then(() => {
                        deleteByPath(cpPath);
                        return [true];
                      })
                      .catch(err => {
                        console.warn(err);
                        res.json({ success: false, msg: 'ERROR_COPING', code: 400, data: {} });
                        deleteByPath(unzipDir);
                        return [false];
                      });
                  }
                  return [true];
                })
                .catch(err => {
                  console.warn(err);
                  res.json({ success: false, msg: 'ERROR_COPING', code: 400, data: {} });
                  deleteByPath(unzipDir);
                  return [false];
                });
            }
            return [true];//can't be directory multiple contents
          })
          .catch(err => {
            console.warn(err);
            res.json({ success: false, msg: 'ERROR_COPING', code: 400, data: {} });
            deleteByPath(unzipDir);
            return [false];
          });
      })
      .catch(err => {
        res.json({ success: false, msg: 'ERROR_EXTRACTING', code: 400, data: {} });
        console.warn(err);
        deleteByPath(fileObject.path);
        deleteByPath(unzipDir);//?
        return [false];
      });
  };

  const checkIndexExists = function (success) {
    if (!success) {
      return [false];
    }
    return fs
      .readdirAsync(unzipDir)
      .then(result => {
        if (!result || !result.length) {
          deleteByPath(unzipDir);
          return [false];
        }
        let indexExists = false;
        result.forEach(el => {
          if (/index.html/i.test(el)) {
            indexExists = true;
            return;
          }
        });
        if (!indexExists) {
          res.json({ success: false, msg: 'NO_INDEX', code: 400, data: {} });
          deleteByPath(unzipDir);
        }
        return [indexExists];
      })
      .catch(err => {
        res.json({ success: false, msg: 'ERROR_EXTRACTING', code: 400, data: {} });
        console.warn(err);
        deleteByPath(fileObject.path);
        return [false, 500];
      });
  };

  const saveToDb = function (success) {
    if (!success) {
      return [false];
    }

    const bizModel = BizModel.newBizModel();

    bizModel.name = formObj.fields.name;
    bizModel.slug = dirSlug;

    return bizModel
      .save()
      .then(savedBizModel => {
        return [true, savedBizModel];
      })
      .catch(err => {
        res.json({ success: false, msg: 'ERROR_SAVING_DB', code: 400, data: {} });
        console.warn(err.message);
        deleteByPath(unzipDir);
      });
  };

  return Promise
    .resolve(parseForm())
    .spread(validateUpload)
    .spread(checkIfNew)
    .spread(extractBizModel)
    .spread(checkIndexExists)
    .spread(saveToDb)
    .catch(err => {
      res.json({ success: false, msg: 'INTERNAL_ERROR', code: 500, data: {} });
      console.warn(err);
      deleteByPath(fileObject.path);
      return [false, 500];
    });
};

//delete only if depbiz has no ref

exports.deleteModel = function (req, res) {
  const id = req.params.bizId;
  return DepBiz.findOne({ 'biz': id }, 'biz')
    .then(depBizFound => {
      if (depBizFound) {
        res.json({ success: false, code: 409 });
        return false;
      }
      return BizModel.findById(id);
    })
    .then(bizModelFound => {
      if (!bizModelFound) {
        res.json({ success: false, code: 404 });
        return [false];
      }
      return [
        bizModelFound,
        fs.removeAsync(path.join(extractDir, bizModelFound.slug))
      ];
    })
    .spread(bizModelFound => {
      if (bizModelFound) {
        return bizModelFound.remove();
      }
      return false;
    })
    .then(() => {
      return true;
    })
    .catch(err => {
      res.json({ success: false, code: 500 });
      console.log(err.message);
      return false;
    });
};
