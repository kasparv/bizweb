const passport = require('passport');
const BasicStrategy = require('passport-http').BasicStrategy;
const User = require('../models/user');
const jwt = require('jsonwebtoken');

passport.use(new BasicStrategy(
  function (username, password, callback) {
    User.findOne({ username: username }, function (err, user) {
      if (err) {
        return callback(err);
      }
      if (!user) {// No user found with that username
        return callback(null, false);
      }
      user.verifyPassword(password, function (err, isMatch) {// Make sure the password is correct
        if (err) {
          return callback(err);
        }
        if (!isMatch) {// Password did not match
          return callback(null, false);
        }

        let token = jwt.sign({ _id: user._id, role: user.role }, '123456', {
          expiresIn: 60 * 60 * 24 // expires in 24 hours
        });

        user.token = token;
        return callback(null, user);// Success
      });
    });
  }
));

/**
 * Login
 */
exports.isAuthenticated = passport.authenticate('basic', { session: false });

exports.logIn = function (req, res) {
  res.json({
    success: true,
    token: req.user.token,
    role: req.user.role
  });
};

exports.hasJWT = function (req, res, next) {
  //var token = req.body.token || req.query.token || req.headers['x-access-token'];
  var token = req.headers['x-access-token'];
  if (token) {
    jwt.verify(token, '123456', function (err, decoded) {// verifies secret and checks exp
      if (err) {
        return res.status(403).json({
          success: false,
          message: 'Failed to authenticate token.'
        });
      } else {
        req.decoded = decoded;// if everything is good, save to request for use in other routes
        next();
      }
    });
  } else {
    return res.status(403).send({
      success: false,
      message: 'No token provided.'
    });
  }
};

exports.isManager = function (req, res, next) {
  //var token = req.body.token || req.query.token || req.headers['x-access-token'];
  var token = req.headers['x-access-token'];
  if (token) {
    jwt.verify(token, '123456', function (err, decoded) {// verifies secret and checks exp
      if (err) {
        return res.status(403).json({
          success: false,
          message: 'Failed to authenticate token.'
        });
      } else {
        if (decoded.role === 'manager') {
          req.decoded = decoded;
          next();
        } else {
          return res.status(403).send({
            success: false,
            message: 'ACL'
          });
        }
      }
    });
  } else {
    return res.status(403).send({
      success: false,
      message: 'No token provided.'
    });
  }
};

exports.publicAccess = function (req, res, next) {
  next();
};
