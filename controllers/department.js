const Promise = require('bluebird');
const Department = require('../models/department');
const Dealer = require('../models/dealer');

/**
 * @api {post} /api/department
 */
exports.postDepartment = function (req, res) {
  if (!req.body.name || !req.body.dealer) {
    return res.status(400).json({ message: 'Invalid', data: {} });
  }
  const department = Department.newDepartment();
  department.name = req.body.name;
  department.dealer = req.body.dealer;
  department
    .save()
    .then(model => {
      Dealer
        .findById(req.body.dealer._id)
        .then(dealer => {
          dealer.departments.push(model);
          dealer
            .save()
            .then(() => {
              res.json({ success: true, data: model, code: 201 });
            })
            .catch(err => {
              res.json({ success: false, code: 500 });
              console.log(err.message);
            });
        });
    })
    .catch(err => {
      res.json({ success: false, code: 500 });
      console.log(err.message);
    });
};

/**
 * @api {get} /api/department
 */
exports.getDepartments = function (req, res) {
  Department
    .find()
    .populate('dealer')
    .then(models => {
      if (models) {
        return res.json({ success: true, data: models, code: 200 });
      }
      res.json({ success: true, data: [], code: 204 });
    })
    .catch(err => {
      res.json({ success: false, code: 500 });
      console.log(err.message);
    });
};

/**
 * @api {put}  /api/department/:departmentId
 */
exports.putDepartment = function (req, res) {
  if (!req.body.name || !req.body.dealer) {
    return res.status(400).json({ message: 'Invalid', data: {} });
  }
  const id = req.params.departmentId;
  Department
    .findById(id)
    .then(currentDepartment => {
      let previousDealerId = currentDepartment.dealer;
      Dealer.findOne(previousDealerId)
        .then(prevDealerFound => {
          prevDealerFound.departments.pull(id);
          prevDealerFound.save()
            .then(() => {
              currentDepartment.name = req.body.name;
              currentDepartment.dealer = req.body.dealer._id;
              currentDepartment.save()
                .then((departmentUpdated) => {
                  Dealer
                    .findById(req.body.dealer._id)
                    .then(currentDealer => {
                      currentDealer.departments.push(departmentUpdated);
                      currentDealer
                        .save()
                        .then(() => {
                          res.json({ success: true, code: 202, data: departmentUpdated });
                        });
                    });
                })
                .catch(err => {
                  res.json({ success: false, code: 500 });
                  console.log(err.message);
                });
            })
            .catch(err => {
              res.json({ success: false, code: 500 });
              console.log(err.message);
            });
        })
        .catch(err => {
          res.json({ success: false, code: 500 });
          console.log(err.message);
        });
    });
};

/**
 * @api {get} /api/department/:departmentId
 */
exports.getDepartment = function (req, res) {
  const id = req.params.departmentId;
  Department
    .findById(id)
    .then(model => {
      if (model) {
        return res.json({ success: true, data: model, code: 200 });
      }
      res.json({ success: false, code: 404 });
    })
    .catch(err => {
      res.json({
        success: false,
        code: 500
      });
      console.log(err.message);
    });
};

/**
 * @api {delete} /api/department/:departmentId
 */
exports.deleteDepartment = function (req, res) {
  const id = req.params.departmentId;
  Department
    .findById(req.params.departmentId)
    .then(department => {
      if (!department) {
        return res.json({ success: false, code: 404 });
      }
      if (department.depbizs.length) {
        return res.json({ success: false, code: 409 });
      }
      Dealer
        .findById(department.dealer)
        .then(dealer => {
          dealer.departments.pull(id);
          dealer.save()
            .then(() => {
              department
                .remove()
                .then(() => {
                  res.json({ success: true, code: 202 });
                })
                .catch(err => {
                  res.json({ success: false, code: 500 });
                  console.log(err.message);
                });
            })
            .catch(err => {
              res.json({ success: false, code: 500 });
              console.log(err.message);
            });
        })
        .catch(err => {
          res.json({ success: false, code: 500 });
          console.log(err.message);
        });
    })
    .catch(err => {
      res.json({ success: false, code: 500 });
      console.log(err.message);
    });
};