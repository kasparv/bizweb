const mongoose = require('mongoose'),
  Schema = mongoose.Schema;

const BizSchema = new mongoose.Schema({
  name: {
    type: String,
    required: true
  },
  slug: {
    type: String,
    required: true
  },
  depbizs: [//Biz hasMany DepBiz
    {type: Schema.Types.ObjectId, ref: 'DepBiz'}
  ]
});

const BizModel = mongoose.model('Biz', BizSchema);
module.exports = BizModel;

module.exports.newBizModel = function () {
  return new BizModel();
};


