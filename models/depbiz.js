const mongoose = require('mongoose'),
  Schema = mongoose.Schema;

const DepBizSchema = new mongoose.Schema({
  department: {//depbiz hasOne Department
    type: Schema.Types.ObjectId, ref: 'Department',
    required: true
  },
  biz: {//depbiz hasOne BizModel
    type: Schema.Types.ObjectId, ref: 'Biz',
    required: true
  }
});

const DepBizModel = mongoose.model('DepBiz', DepBizSchema);

module.exports = DepBizModel;

module.exports.newDepBiz = function () {
  return new DepBizModel();
};


