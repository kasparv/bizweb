const mongoose = require('mongoose'),
  Schema = mongoose.Schema;

const DepartmentSchema = new mongoose.Schema({
  name: {
    type: String,
    required: true
  },
  dealer: {//Department hasOne Dealer
    type: Schema.Types.ObjectId, ref: 'Dealer',
    required: true
  },
  depbizs: [//Department hasMany DepBiz
    {type: Schema.Types.ObjectId, ref: 'DepBiz'}
  ]
});

const DepartmentModel = mongoose.model('Department', DepartmentSchema);

module.exports = DepartmentModel;

module.exports.newDepartment = function () {
  return new DepartmentModel();
};


