const mongoose = require('mongoose'),
  Schema = mongoose.Schema;

const DealerSchema = new mongoose.Schema({
  name: {
    type: String,
    required: true
  },
  departments: [{type: Schema.Types.ObjectId, ref: 'Department'}]//Dealer has Many Departments 
});

const DealerModel = mongoose.model('Dealer', DealerSchema);
module.exports = DealerModel;

module.exports.newDealer = function () {
  return new DealerModel();
};


