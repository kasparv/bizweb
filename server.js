
const port = process.env.PORT || 3333;// Use environment defined port or 3333

const express = require('express');// Get the packages we need
const app = express();// Create our Express application
const mongoose = require('mongoose');
const bodyParser = require('body-parser');//accept data via POST or PUT
const Promise = require('bluebird');
const router = require('./routers/routes');

const passport = require('passport');

mongoose.Promise = require('bluebird');//http://mongoosejs.com/docs/promises.html
mongoose.connect('mongodb://localhost:27017/bizweb');

app.use(bodyParser.json());
app.use(bodyParser.urlencoded({
  extended: true
}));

// Add headers - http://stackoverflow.com/questions/18310394/no-access-control-allow-origin-node-apache-port-issue
app.use(function (req, res, next) {
  // Website you wish to allow to connect
  res.setHeader('Access-Control-Allow-Origin', 'http://localhost:4200');
  // Request methods you wish to allow
  res.setHeader('Access-Control-Allow-Methods', 'GET, POST, OPTIONS, PUT, PATCH, DELETE');
  // Request headers you wish to allow
  res.setHeader('Access-Control-Allow-Headers', 'X-Requested-With,content-type,Authorization,x-access-token');
  // Set to true if you need the website to include cookies in the requests sent
  // to the API (e.g. in case you use sessions)
  res.setHeader('Access-Control-Allow-Credentials', true);
  // Pass to next layer of middleware
  next();
});

app.use('/', express.static(__dirname + '/ng2-front/dist'));

app.use('/api', router);

app.use('/m', express.static(__dirname + '/public'));

app.listen(port);

console.log('bizweb started ' + port);